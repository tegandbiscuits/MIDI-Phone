class: center, middle
# MIDI Phone

???
Hi, I've got this phone I wanted to show off.
I modified it to be a MIDI controller.

---
class: center, middle
# Demonstration

???
I'll jump right and and show you it in action.
I'm not musically inclined, so don't expect this to be good.

The program I'm using is LMMS.

---
class: center, middle
# What is MIDI

???
I want to talk a bit about what MIDI actually is.

---
## MIDI

* A protocol allowing musical devices to communicate
* Is not concerned with what the music sounds like

???
Basically, MIDI is just a protocol that helps musical devices talk to each other.

A lot of people think of MIDI as a sort of chiptune music, but the fact that it sounds like a plastic recorder really has nothing to do with MIDI, just what's playing it.

---
## Use of MIDI

* Music production
  - Recorded as notes, not waves
* Instruments controlling instruments
* Stage effects

???
One of the biggest uses for MIDI is producing music.
Rather than recording the analog sound, a musician can plug a MIDI controller into a computer, and record it as notes.
The benefit to that is it's easier to go back and fix mistakes, and control what the instrument sounds like.

MIDI also provides a way for instruments to control other instruments.
As well as triggering things like stage lights on a cue in a song.

---
class: center, middle
# What MIDI looks like

MIDI isn't a complicated protocol, and it's neat to see what how it actually works.
So I'm going to briefly show it's technical side.

---
## MIDI Message

* 1-3 bytes
* Command byte
  - Required
* Data byte
  - Depends on the command

???
A MIDI message has 1-3 bytes.
It will always have a command byte.
Depending on the command, will have 0, 1, or 2 data bytes to give more detail.

---
## Command Byte

* First byte in message
* Says what should be done and the channel
* Starts with 1 in binary

| Binary   | Hex  | Integer |
| -------- | ---- | ------- |
| 10000000 | 0x80 | 128     |
| 11111111 | 0xFF | 255     |


???
The command byte says what shold be done.
The first bit will start with a 1.
The first four bits specify the action, and the last four specify the channel
A channel is just a way to separate things, kind of like a walkie talkie.

---
## Data Byte

* Amount depends on command
* Give values for commands
* Starts with 0 in binary

| Binary   | Hex  | Integer |
| -------- | ---- | ------- |
| 00000000 | 0x00 | 0       |
| 01111111 | 0x7F | 127     |


???
The data bytes are used to provide values for a command's parameters.
The first bit will start with 0.

---
## Example Message

| Command            | Param 1  | Param 2      |
| ------------------ | -------- | ------------ |
| Note On, Channel 1 | Key 60   | Velocity 127 |
| 0x91               | 0x3C     | 0x7F         |
| 10010001           | 00111100 | 01111111     |

| Command             | Param 1  | Param 2              |
| ------------------- | -------- | -------------------- |
| Note Off, Channel 1 | Key 60   | Release Velocity 127 |
| 0x81                | 0x3C     | 0x7F                 |
| 10000001            | 00111100 | 01111111             |

???
So if you play middle c really hard on a keyboard, this is what's being sent.

First note 60 is turned on with 127 velocity.

Normally the note will play forever unless it's turned off.
So when the key is released, note 60 is turned off with release velocity 127.

This example uses the second channel.
The key and channel need to be the same, but the velocity doesn't matter.

---
class: center, middle
# Build Process

---
![Picture of inside](./photos/inside.jpg)

???
The only parts in the phone is the dialpad, a potentiometer, and a knockoff Arduino.

The potentiometer is used to set modulation, but depending on the program it's controlling, can be reassigned.

---
![Picture of broken switch](./photos/broken-hook-switch.jpg)

???
It looked like it might be possible to use the hook switch for modulation.
But when I took it off to get a better look, it fell apart.
So I went with a potentiometer.

---
![Original internals sans keypad](./photos/original-internals-sans-keypad.jpg)

???
The keypad isn't attatched here, but this is what it looked like originally.

---
class: center, middle
# Source

[https://gitlab.com/NateThePirate/MIDI-Phone](https://gitlab.com/NateThePirate/MIDI-Phone)

---
class: center, middle
# Links About MIDI

[https://www.cs.cmu.edu/~music/cmsip/readings/MIDI%20tutorial%20for%20programmers.html](https://www.cs.cmu.edu/~music/cmsip/readings/MIDI%20tutorial%20for%20programmers.html)

[https://ccrma.stanford.edu/~craig/articles/linuxmidi/misc/essenmidi.html](https://ccrma.stanford.edu/~craig/articles/linuxmidi/misc/essenmidi.html)
